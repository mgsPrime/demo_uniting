<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Index</title>
    <link rel="stylesheet" href="css/index.css">
</head>
<body>
    <img id="back" src="img/home/SVG/background.svg" alt="">
    <div id="content">
        <div id="side">
            <div class="burger">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line" id="l3"></div>
            </div>
            <div class="network">
                <a href="" class="socials">
                    <img src="img/home/SVG/insta.svg" alt="">
                </a>
                <a href="" class="socials">
                    <img src="img/home/SVG/twit.svg" alt="">
                </a>
                <a href="" class="socials">
                    <img src="img/home/SVG/fb.svg" alt="">
                </a>



            </div>
        </div>
        <div class="container">
            <div id="front">
                <img id="logo" src="img/home/SVG/logo.svg" alt="">
                <div class="text">
                    <p>Pour gérer vos projets et vos équipes avec Unii, votre nouvel assitant intelligent !</p>
                    <div id="buttons">
                        <button class="button" id="b1">
                            S'INSCRIRE
                        </button>
                        <button class="button" id="b2">
                            <a>EN SAVOIR PLUS</a>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</body>
</html>